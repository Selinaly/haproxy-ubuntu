# Setting up load balancer using HAProxy on Ubuntu Machine

1. Logging into Ubuntu Machine
```
$ ssh -i ~/.ssh/SelinaLyKey.pem ubuntu@<IP Address>
```
2. Install software and HAProxy
```
$ sudo apt-get install --no-install-recommends software-properties-common
$ sudo add-apt-repository ppa:vbernat/haproxy-2.2
$ sudo apt-get install haproxy=2.2.\* -y
$ sudo apt update
$ sudo systemctl start haproxy
```
3. Show list of ports and sockets 
```
$ sudo netstat -tlnp
> Active Internet connections (only servers)...
```
4. HAProxy.cfg
```
$ cd /etc/haproxy/
$ sudo nano haproxy.cfg
>  global
    log 127.0.0.1 local0 notice
    maxconn 2000
    user haproxy
    group haproxy

defaults
    log     global
    mode    http
    option  httplog
    option  dontlognull
    retries 3
    option redispatch
    timeout connect  5000
    timeout client  10000
    timeout server  10000

frontend simple_load_balancer
    bind *:80
    mode http
    stats enable
    stats uri /haproxy?stats
    stats realm Strictly\ Private
    stats auth A_Username:Admin
    stats auth Another_User:Admin
    default_backend simple_servers

backend simple_servers
    balance roundrobin
    option httpclose
    option forwardfor
    server lamp1 Private IP Address check
    server lamp2 Private IP Address check
$ sudo systemctl start haproxy
$ haproxy -f haproxy.cfg (checks if haproxy is working)
```
