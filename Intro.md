# Load balancer using HAProxy

How to set up a load balancer with HAProxy on a Ubuntu machine.

## What is a HAProxy?
HAProxy = High Avaliability Proxy
It is an open source software TCP/HTTP Load balancer which can be run on Linux 

### Common terms
- ACLs 
  ACLs are used to test some condition and perform an action based on the test result (eg. select a server, or block a request) 
- Backend
  A backend is a set of servers that receives forwarded requests. Backends are defined in the backend section of the HAProxy configuration. The basic definition of backend is form:
  - which load balance algorithm to use
  - a list of servers and ports

A backend can contain one or many servers in it, adding more servers to your backend will increase your potential load capacity by spreading the load over multiple servers. Thus increasing reliability, in case some of your backend servers become unavailable.
- Frontend
  A frontend defines how requests should be forwarded to backends. Frontends are defined in the frontend section of the HAProxy configuration. Their definitions are composed of the following components:
  - a set of IP addresses and a port (e.g. 10.1.1.7:80, *:443, etc.)
  - ACLs
  - use_backend rules, which define which backends to use depending on which ACL conditions are matched, and/or a default_backend rule that handles every other case

## What is a load balancer?

A load balancer refers to efficiently distributing incoming network traffic across a group of backend servers, also known as a server farm or server pool.
It acts as the “traffic cop” sitting in front of your servers, routing client requests across all servers capable of fulfilling those requests. Maximising speed and capacity utilization and ensures that no one server is overworked, which could degrade performance. If a single server goes down, the load balancer redirects traffic to the remaining online servers. When a new server is added to the server group, the load balancer automatically starts to send requests to it.